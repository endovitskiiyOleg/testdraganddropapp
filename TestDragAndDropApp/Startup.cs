﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestDragAndDropApp.Startup))]
namespace TestDragAndDropApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
